# Lab4

Требования:
- Java 17

## Сборка

```shell
mvn clean package
```

## Запуск

### Сервер

```shell
java -jar lab4-server/target/lab4-server-jar-with-dependencies.jar
```

### Клиент

```shell
java -jar lab4-client/target/lab4-client-jar-with-dependencies.jar
```