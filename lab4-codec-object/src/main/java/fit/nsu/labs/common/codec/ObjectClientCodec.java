package fit.nsu.labs.common.codec;

import fit.nsu.labs.common.dto.Command;
import fit.nsu.labs.common.exception.DecoderException;
import fit.nsu.labs.common.exception.EncoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class ObjectClientCodec implements Encoder<Command>, Decoder<Command> {
    private static final Logger log = LoggerFactory.getLogger(ObjectClientCodec.class);

    @Override
    public byte[] encode(Command object) throws EncoderException {
        log.debug("[client.codec] outgoing message {}", object.getClass().getSimpleName());
        return ObjectCodecUtil.encode(object);
    }

    @Override
    public Command decode(InputStream inputStream) throws DecoderException {
        var result = (Command) ObjectCodecUtil.decoder(inputStream);
        log.debug("[client.codec] input message {}", result.getClass().getSimpleName());
        return result;
    }
}
