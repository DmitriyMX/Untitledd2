package fit.nsu.labs.common.codec;

import fit.nsu.labs.common.exception.DecoderException;
import fit.nsu.labs.common.exception.EncoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class ObjectServerCodec implements Encoder<XmlDto>, Decoder<XmlDto> {
    private static final Logger log = LoggerFactory.getLogger(ObjectServerCodec.class);

    @Override
    public byte[] encode(XmlDto object) throws EncoderException {
        log.debug("[server.codec] outgoing message {}", object.getClass().getSimpleName());
        return ObjectCodecUtil.encode(object);
    }

    @Override
    public XmlDto decode(InputStream inputStream) throws DecoderException {
        var result = (XmlDto) ObjectCodecUtil.decoder(inputStream);
        log.debug("[server.codec] input message {}", result.getClass().getSimpleName());
        return result;
    }
}
