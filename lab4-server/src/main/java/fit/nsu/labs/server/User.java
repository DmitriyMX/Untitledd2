package fit.nsu.labs.server;

import io.netty.channel.ChannelHandlerContext;

public record User(String name, ChannelHandlerContext channelContext) {
}
