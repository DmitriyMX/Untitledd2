package fit.nsu.labs.server.netty;

import io.netty.util.AttributeKey;

public interface Attributes {
    AttributeKey<Integer> ATTRIBUTE_SESSION_ID = AttributeKey.newInstance("ATTRIBUTE_SESSION_ID");
}
