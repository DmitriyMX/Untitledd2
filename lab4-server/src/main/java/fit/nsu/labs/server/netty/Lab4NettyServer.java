package fit.nsu.labs.server.netty;

import com.google.inject.Guice;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import fit.nsu.labs.Configuration;
import fit.nsu.labs.server.UserManager;
import fit.nsu.labs.server.di.ServerModule;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Lab4NettyServer {
    private static final Logger log = LoggerFactory.getLogger(Lab4NettyServer.class);
    private static final int SO_BACKLOG_VALUE = 1024; //MAGIC_NUMBER

    public void start(int port, int countLastMessages) {
        var injector = Guice.createInjector(new ServerModule(countLastMessages));

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap
                    .option(ChannelOption.SO_BACKLOG, SO_BACKLOG_VALUE)
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.DEBUG))
                    .childHandler(injector.getInstance(NettyChannelInit.class));

            serverBootstrap.bind(port).sync().channel().closeFuture().sync();
        } catch (InterruptedException e) {
            log.error("{}", e.getMessage(), e);
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) {
        var configuration = new Configuration();

        log.info("SERVER PORT: {}", configuration.getPort());
        log.info("COUNT_MSG: {}", configuration.getCountMessages());

        new Lab4NettyServer().start(configuration.getPort(), configuration.getCountMessages());
    }
}
