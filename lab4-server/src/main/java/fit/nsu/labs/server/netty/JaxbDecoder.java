package fit.nsu.labs.server.netty;

import fit.nsu.labs.common.dto.Command;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import jakarta.xml.bind.JAXBContext;

import java.io.ByteArrayInputStream;
import java.util.List;

public class JaxbDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> list) throws Exception {
        var length = byteBuf.readInt();
        var xmlBytes = new byte[length];
        byteBuf.readBytes(xmlBytes);

        //TODO избавиться от постоянного создания инстанса
        var command = (Command) JAXBContext.newInstance(Command.class)
                .createUnmarshaller()
                .unmarshal(new ByteArrayInputStream(xmlBytes));
        list.add(command);
    }
}
