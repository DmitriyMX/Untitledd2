package fit.nsu.labs.server.netty;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

@Singleton
public class NettyChannelInit extends ChannelInitializer<SocketChannel> {
    private final Provider<NettyHandler> providerNettyHandler;

    @Inject
    public NettyChannelInit(Provider<NettyHandler> providerNettyHandler) {
        this.providerNettyHandler = providerNettyHandler;
    }

    @Override
    protected void initChannel(SocketChannel channel) {
        channel.pipeline()
                .addLast(new LoggingHandler(LogLevel.DEBUG))
                .addLast(new JaxbDecoder())
                .addLast(new JaxbEncoder())
                .addLast(providerNettyHandler.get());
    }
}
