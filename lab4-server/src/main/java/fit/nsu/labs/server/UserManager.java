package fit.nsu.labs.server;

import com.google.inject.Singleton;
import fit.nsu.labs.common.codec.XmlDto;
import fit.nsu.labs.server.exception.UserExistsException;
import fit.nsu.labs.server.netty.Attributes;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Singleton
public class UserManager {
    // sessionId, User
    private final ConcurrentMap<Integer, User> users = new ConcurrentHashMap<>();

    public void createUser(String name, ChannelHandlerContext channelHandlerContext)
            throws UserExistsException {

        if (users.values().stream().anyMatch(u -> u.name().equalsIgnoreCase(name))) {
            throw new UserExistsException();
        }

        var user = new User(name, channelHandlerContext);
        users.put(channelHandlerContext.channel().attr(Attributes.ATTRIBUTE_SESSION_ID).get(), user);
    }

    public List<User> listUsers() {
        return List.copyOf(users.values());
    }

    public Optional<User> getBySessionId(int sessionId) {
        return Optional.ofNullable(users.get(sessionId));
    }

    public void broadcastSend(XmlDto xmlDto) {
        users.values().forEach(u -> u.channelContext().channel().writeAndFlush(xmlDto));
    }

    public void logout(int sessionId) {
        if (!users.containsKey(sessionId)) return;
        users.remove(sessionId);
    }
}
