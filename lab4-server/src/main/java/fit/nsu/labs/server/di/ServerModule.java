package fit.nsu.labs.server.di;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import fit.nsu.labs.common.TextMessage;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class ServerModule extends AbstractModule {
    private final int countLastMessages;

    public ServerModule(int countLastMessages) {
        this.countLastMessages = countLastMessages;
    }

    @Override
    protected void configure() {
        bind(Integer.class).annotatedWith(Names.named("countLastMessages")).toInstance(countLastMessages);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    static BlockingDeque<TextMessage> provideLastMessages(@Named("countLastMessages") int countLastMessages) {
        return new LinkedBlockingDeque<>(countLastMessages);
    }
}
