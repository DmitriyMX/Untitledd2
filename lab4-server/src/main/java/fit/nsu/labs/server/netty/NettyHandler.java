package fit.nsu.labs.server.netty;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import fit.nsu.labs.common.TextMessage;
import fit.nsu.labs.common.dto.*;
import fit.nsu.labs.common.dto.Error;
import fit.nsu.labs.server.UserManager;
import fit.nsu.labs.server.exception.UserExistsException;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class NettyHandler extends SimpleChannelInboundHandler<Command> {
    private static final Logger log = LoggerFactory.getLogger(NettyHandler.class);
    private static final AtomicInteger SESSION_GENERATOR = new AtomicInteger(1);

    private final UserManager userManager;
    private final BlockingDeque<TextMessage> lastMessages;
    private final int countLastMessages;

    @Inject
    public NettyHandler(UserManager userManager, BlockingDeque<TextMessage> lastMessages,
                        @Named("countLastMessages") int countLastMessages) {
        this.userManager = userManager;
        this.lastMessages = lastMessages;
        this.countLastMessages = countLastMessages;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Command command) throws Exception {
        switch (command.getNameAttribute()) {
            case LOGIN -> handleLogin(ctx, command);
            case MESSAGE -> handleIncomingMessage(ctx, command);
            case LIST -> handleListMembers(ctx);
            case LOGOUT -> handleLogout(ctx, command);
            default -> log.warn("Unsupported command '{}'", command.getNameAttribute());
        }
    }

    private void handleLogin(ChannelHandlerContext ctx, Command command) {
        log.debug("[handler login]");

        try {
            var sessionId = SESSION_GENERATOR.getAndIncrement();
            ctx.channel().attr(Attributes.ATTRIBUTE_SESSION_ID).set(sessionId);
            userManager.createUser(command.getName(), ctx);

            var success = new Success();
            success.setSession(String.valueOf(sessionId));
            ctx.channel().write(success);

            if (!lastMessages.isEmpty()) {
                for (TextMessage message : lastMessages) {
                    var event = new Event();
                    event.setNameAttribute(EventEnum.MESSAGE);
                    event.setMessage(message.text());
                    event.setName(message.name());
                    ctx.channel().write(event);
                }
            }
        } catch (UserExistsException e) {
            var error = new Error();
            error.setMessage("Пользователь с именем '%s' уже существует".formatted(command.getName()));
            ctx.channel().write(error);
        }

        ctx.channel().flush();
    }

    private void handleIncomingMessage(ChannelHandlerContext ctx, Command command) throws InterruptedException {
        log.debug("[handle Message]");

        var user = userManager.getBySessionId(ctx.channel().attr(Attributes.ATTRIBUTE_SESSION_ID).get())
                .orElseThrow(() -> new RuntimeException("ЧО? КУДА ПОЛЬЗОВАТЕЛЯ ДЕЛИ???"));

        var textMessage = new TextMessage(user.name(), command.getMessage());
        if (lastMessages.size() == countLastMessages) lastMessages.removeFirst();
        lastMessages.putLast(textMessage);

        var event = new Event();
        event.setNameAttribute(EventEnum.MESSAGE);
        event.setMessage(textMessage.text());
        event.setName(textMessage.name());

        userManager.broadcastSend(event);
    }

    private void handleListMembers(ChannelHandlerContext ctx) {
        log.debug("[handle ListMembers]");

        var listusers = new Success.Listusers();
        listusers.getUser().addAll(userManager.listUsers().stream()
                .map(u -> {
                    var user = new User();
                    user.setName(u.name());
                    user.setType(String.valueOf(u.channelContext().channel().attr(Attributes.ATTRIBUTE_SESSION_ID).get()));
                    return user;
                })
                .toList());

        var success = new Success();
        success.setListusers(listusers);

        ctx.channel().writeAndFlush(success);
    }

    private void handleLogout(ChannelHandlerContext ctx, Command command) {
        log.debug("[handle Logout]");

        var sessionId = ctx.channel().attr(Attributes.ATTRIBUTE_SESSION_ID).get();

        if (!command.getSession().equals(String.valueOf(sessionId))) {
            var error = new Error();
            error.setMessage("Указанная сессия отличается от сессии пользователя");
            ctx.channel().writeAndFlush(error);
        } else {
            userManager.logout(sessionId);
            ctx.channel().writeAndFlush(new Success());
            ctx.channel().close();
        }
    }
}
