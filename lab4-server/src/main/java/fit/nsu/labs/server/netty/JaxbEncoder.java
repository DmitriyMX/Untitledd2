package fit.nsu.labs.server.netty;

import fit.nsu.labs.common.codec.XmlDto;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;

import java.io.ByteArrayOutputStream;

public class JaxbEncoder extends MessageToByteEncoder<XmlDto> {

    @Override
    protected void encode(ChannelHandlerContext ctx, XmlDto xmlDto, ByteBuf byteBuf) throws Exception {
        var baos = new ByteArrayOutputStream();

        var marshaller = JAXBContext.newInstance(xmlDto.getClass()).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.marshal(xmlDto, baos);

        byteBuf.writeInt(baos.size());
        byteBuf.writeBytes(baos.toByteArray());
    }
}
