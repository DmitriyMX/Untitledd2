package fit.nsu.labs.client;

import fit.nsu.labs.common.dto.Command;

public record OutgoingQueueItem(Command commandDto, Runnable afterTask) {
}
