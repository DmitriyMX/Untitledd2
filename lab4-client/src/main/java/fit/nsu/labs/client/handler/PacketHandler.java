package fit.nsu.labs.client.handler;

import fit.nsu.labs.client.IncomingQueueItem;
import fit.nsu.labs.common.dto.Error;
import fit.nsu.labs.common.dto.Event;
import fit.nsu.labs.common.dto.Success;

import java.util.concurrent.BlockingQueue;

public class PacketHandler extends Thread {
    private final BlockingQueue<IncomingQueueItem> incomingQueue;
    private final SuccessHandler successHandler;
    private final EventHandler eventHandler;
    private final ErrorHandler errorHandler;

    public PacketHandler(BlockingQueue<IncomingQueueItem> incomingQueue,
                         SuccessHandler successHandler, EventHandler eventHandler, ErrorHandler errorHandler) {
        this.incomingQueue = incomingQueue;
        this.successHandler = successHandler;
        this.eventHandler = eventHandler;
        this.errorHandler = errorHandler;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                var queueItem = incomingQueue.take();

                if (queueItem.xmlDto().getClass().equals(Success.class)) {
                    successHandler.handle((Success) queueItem.xmlDto());
                } else if (queueItem.xmlDto().getClass().equals(Event.class)) {
                    eventHandler.handle((Event) queueItem.xmlDto());
                } else if (queueItem.xmlDto().getClass().equals(Error.class)) {
                    errorHandler.handle((Error) queueItem.xmlDto());
                } else {
                    System.err.printf("UNSUPPORTED DTO %s\n", queueItem.xmlDto().getClass());
                }
            }
        } catch (InterruptedException ignore) {
            // просто выходим
        }
    }
}
