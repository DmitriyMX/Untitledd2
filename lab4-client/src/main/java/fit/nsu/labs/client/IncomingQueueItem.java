package fit.nsu.labs.client;

import fit.nsu.labs.common.codec.XmlDto;

public record IncomingQueueItem(XmlDto xmlDto, ServerConnection connection) {
}
