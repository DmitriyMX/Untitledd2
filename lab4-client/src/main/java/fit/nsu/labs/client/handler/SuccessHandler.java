package fit.nsu.labs.client.handler;

import fit.nsu.labs.client.Lab4Client;
import fit.nsu.labs.client.MainWindow;
import fit.nsu.labs.client.model.Event;
import fit.nsu.labs.common.dto.Success;
import fit.nsu.labs.common.dto.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SuccessHandler {
    private static final Logger log = LoggerFactory.getLogger(SuccessHandler.class);

    private final MainWindow mainWindow;
    private final Lab4Client lab4Client;

    public SuccessHandler(MainWindow mainWindow, Lab4Client lab4Client) {
        this.mainWindow = mainWindow;
        this.lab4Client = lab4Client;
    }

    public void handle(Success success) {
        if (success.getSession() != null) {
            handleLoginResponse(success);
        } else if (success.getListusers() != null) {
            handleListMembers(success);
        }
    }

    private void handleLoginResponse(Success success) {
        log.debug("[handler.LoginResponse]");

        lab4Client.setSessionId(Integer.parseInt(success.getSession()));
    }

    private void handleListMembers(Success success) {
        log.debug("[handler.SrvListMembers]");

        mainWindow.notification(new Event(Event.EventType.MEMBERS_UPDATED,
                success.getListusers().getUser().stream()
                        .map(User::getName)
                        .toList()));
    }
}
