package fit.nsu.labs.client;

import fit.nsu.labs.client.handler.ErrorHandler;
import fit.nsu.labs.client.handler.EventHandler;
import fit.nsu.labs.client.handler.PacketHandler;
import fit.nsu.labs.client.handler.SuccessHandler;
import fit.nsu.labs.common.codec.Decoder;
import fit.nsu.labs.common.codec.Encoder;
import fit.nsu.labs.common.codec.XmlDto;
import fit.nsu.labs.common.dto.Command;
import fit.nsu.labs.common.dto.CommandEnum;

import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Lab4Client {
    private final BlockingQueue<IncomingQueueItem> incomingQueue = new LinkedBlockingQueue<>();
    private final BlockingQueue<OutgoingQueueItem> outgoingQueue = new LinkedBlockingQueue<>();

    private final String host;
    private final int port;
    private final Encoder<Command> encoder;
    private final Decoder<XmlDto> decoder;

    private ServerConnection connection;
    private int sessionId;

    public Lab4Client(String host, int port, Encoder<Command> encoder, Decoder<XmlDto> decoder) {
        this.host = host;
        this.port = port;
        this.encoder = encoder;
        this.decoder = decoder;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public void connect(MainWindow mainWindow) {
        try {
            var handler = new PacketHandler(incomingQueue,
                    new SuccessHandler(mainWindow, this),
                    new EventHandler(mainWindow),
                    new ErrorHandler(mainWindow)
            );
            handler.start();

            connection = new ServerConnection(new Socket(host, port), incomingQueue, outgoingQueue, decoder, encoder);
            connection.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    public void close() {
        connection.close();
    }

    public void login(String name) {
        var command = new Command();
        command.setNameAttribute(CommandEnum.LOGIN);
        command.setName(name);
        connection.send(command);
    }

    public void requestListMembers() {
        var command = new Command();
        command.setNameAttribute(CommandEnum.LIST);
        command.setSession(String.valueOf(sessionId));
        connection.send(command);
    }

    public void sendTextMessage(String text) {
        var command = new Command();
        command.setNameAttribute(CommandEnum.MESSAGE);
        command.setMessage(text);
        command.setSession(String.valueOf(sessionId));
        connection.send(command);
    }

    public void logout(Runnable after) {
        var command = new Command();
        command.setNameAttribute(CommandEnum.LOGOUT);
        command.setSession(String.valueOf(sessionId));
        connection.send(command, after);
    }
}
