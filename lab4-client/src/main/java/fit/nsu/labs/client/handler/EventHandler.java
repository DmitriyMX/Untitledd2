package fit.nsu.labs.client.handler;

import fit.nsu.labs.client.MainWindow;
import fit.nsu.labs.common.TextMessage;
import fit.nsu.labs.common.dto.Event;

import java.util.List;

public class EventHandler {
    private final MainWindow mainWindow;

    public EventHandler(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void handle(Event event) {
        switch (event.getNameAttribute()) {
            case MESSAGE -> handleNewMessage(event);
            case USERLOGIN -> handleUserLogin(event);
            case USERLOGOUT -> handleUserLogout(event);
        }
    }

    private void handleNewMessage(Event event) {
        var textMessage = new TextMessage(event.getName(), event.getMessage());

        mainWindow.notification(new fit.nsu.labs.client.model.Event(
                fit.nsu.labs.client.model.Event.EventType.MESSAGE_UPDATED,
                List.of(textMessage.toString())));
    }

    private void handleUserLogin(Event event) {
        mainWindow.notification(new fit.nsu.labs.client.model.Event(
                fit.nsu.labs.client.model.Event.EventType.MESSAGE_UPDATED,
                List.of("<%s join server>".formatted(event.getName()))));
    }

    private void handleUserLogout(Event event) {
        mainWindow.notification(new fit.nsu.labs.client.model.Event(
                fit.nsu.labs.client.model.Event.EventType.MESSAGE_UPDATED,
                List.of("<%s leave server>".formatted(event.getName()))));
    }
}
