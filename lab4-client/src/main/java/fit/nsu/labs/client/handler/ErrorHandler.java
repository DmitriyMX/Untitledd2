package fit.nsu.labs.client.handler;

import fit.nsu.labs.client.MainWindow;
import fit.nsu.labs.common.dto.Error;

import java.util.List;

public class ErrorHandler {
    private final MainWindow mainWindow;

    public ErrorHandler(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void handle(Error error) {
        mainWindow.notification(new fit.nsu.labs.client.model.Event(
                fit.nsu.labs.client.model.Event.EventType.MESSAGE_UPDATED,
                List.of("<ERROR: %s>".formatted(error.getMessage()))));
    }
}
