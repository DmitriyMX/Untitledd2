package fit.nsu.labs.client;

import fit.nsu.labs.common.codec.Decoder;
import fit.nsu.labs.common.codec.DumpUtil;
import fit.nsu.labs.common.codec.Encoder;
import fit.nsu.labs.common.codec.XmlDto;
import fit.nsu.labs.common.dto.Command;
import fit.nsu.labs.common.exception.DecoderException;
import fit.nsu.labs.common.exception.EncoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ServerConnection {
    private static final Logger log = LoggerFactory.getLogger(ServerConnection.class);

    private final Socket socket;

    private final BlockingQueue<IncomingQueueItem> incomingQueue;
    private final BlockingQueue<OutgoingQueueItem> outgoingQueue;
    private final Decoder<XmlDto> decoder;
    private final Encoder<Command> encoder;

    private final Thread in;
    private final Thread out;

    public ServerConnection(Socket socket,
                            BlockingQueue<IncomingQueueItem> incomingQueue,
                            BlockingQueue<OutgoingQueueItem> outgoingQueue,
                            Decoder<XmlDto> decoder, Encoder<Command> encoder) {
        this.socket = socket;
        this.incomingQueue = incomingQueue;
        this.outgoingQueue = outgoingQueue;
        this.decoder = decoder;
        this.encoder = encoder;

        this.in = new Thread(this::incoming, "Input handler");
        this.out = new Thread(this::outgoing, "Output handler");

    }

    public void start() {
        log.debug("[connection] open");
        in.start();
        out.start();
    }
    
    private void incoming() {
        log.debug("[connection.incoming] begin loop");
        try {
            while (!Thread.interrupted()) {
                try {
                    incomingQueue.add(new IncomingQueueItem(
                            decoder.decode(socket.getInputStream()),
                            this));
                } catch (DecoderException e) {
                    if (e.getCause() instanceof EOFException) {
                        // Что-то пошло не так. Закрываем все соединения.
                        this.close();
                        break;
                    }

                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            //TODO Что-то со связью с сервером.
            throw new RuntimeException(e);
        } finally {
            log.debug("[connection.incoming] end loop");
        }
    }

    private void outgoing() {
        try {
            log.debug("[connection.outgoing] begin loop");
            while (!Thread.interrupted()) {
                try {
                    var queueItem = outgoingQueue.take();
                    var commandDto = queueItem.commandDto();
                    var outputStream = socket.getOutputStream();

                    byte[] bytes = encoder.encode(commandDto);
                    //TODO в ENV сделать выключатель для дампов
                    DumpUtil.dumpPacket(bytes, "Dump outgoing packet");

                    outputStream.write(bytes);
                    outputStream.flush();
                    if (queueItem.afterTask() != null) {
                        new Thread(queueItem.afterTask()).start();
                    }
                } catch (EncoderException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException ignore) {
            // просто выходим
        } catch (IOException e) {
            //TODO Что-то со связью с сервером.
            throw new RuntimeException(e);
        } finally {
            log.debug("[connection.outgoing] end loop");
        }
    }

    public void send(Command message) {
        send(message, null);
    }

    public void send(Command command, Runnable afterTask) {
        outgoingQueue.add(new OutgoingQueueItem(command, afterTask));
    }

    public void close() {
        log.debug("[connection] close");
        if (!in.isInterrupted()) in.interrupt();
        if (!out.isInterrupted()) out.interrupt();
        try {
            if (!socket.isClosed()) socket.close();
        } catch (IOException ignore) {
            //игнорируем
        }
    }
}
