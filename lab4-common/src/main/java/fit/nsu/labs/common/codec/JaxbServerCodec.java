package fit.nsu.labs.common.codec;

import fit.nsu.labs.common.dto.Error;
import fit.nsu.labs.common.dto.Event;
import fit.nsu.labs.common.dto.Success;
import fit.nsu.labs.common.exception.DecoderException;
import fit.nsu.labs.common.exception.EncoderException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;

public class JaxbServerCodec implements Encoder<XmlDto>, Decoder<XmlDto> {
    private static final Logger log = LoggerFactory.getLogger(JaxbServerCodec.class);

    @Override
    public XmlDto decode(InputStream inputStream) throws DecoderException {
        try {
            var dis = new DataInputStream(inputStream);
            var length = dis.readInt();
            var xmlBytes = dis.readNBytes(length);

            DumpUtil.dumpPacket(xmlBytes, "Dump incoming XML packet without first 4 bytes (length)");

            var document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new ByteArrayInputStream(xmlBytes));

            var rootElement = document.getDocumentElement();
            var rootTagName = rootElement.getTagName().toLowerCase();

            XmlDto result;
            switch (rootTagName) {
                case "error" -> {
                    result = (Error) JAXBContext.newInstance(Error.class).createUnmarshaller()
                            .unmarshal(new ByteArrayInputStream(xmlBytes));
                }
                case "success" -> {
                    result = (Success) JAXBContext.newInstance(Success.class).createUnmarshaller()
                            .unmarshal(new ByteArrayInputStream(xmlBytes));
                }
                case "event" -> {
                    result = (Event) JAXBContext.newInstance(Event.class).createUnmarshaller()
                            .unmarshal(new ByteArrayInputStream(xmlBytes));
                }
                default -> throw new DecoderException("Unknown first xml tag: " + rootTagName);
            }

            log.debug("[client.codec] input message {}", result.getClass().getSimpleName());
            return result;
        } catch (Exception e) {
            throw new DecoderException(e);
        }
    }

    @Override
    public byte[] encode(XmlDto dto) throws EncoderException {
        log.debug("[server.codec] outgoing message {}", dto.getClass().getSimpleName());

        try {
            var baos = new ByteArrayOutputStream();

            var marshaller = JAXBContext.newInstance(dto.getClass()).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(dto, baos);

            byte[] bytes = baos.toByteArray();

            baos = new ByteArrayOutputStream();
            var dos = new DataOutputStream(baos);
            dos.writeInt(bytes.length);
            dos.write(bytes);
            dos.flush();

            return baos.toByteArray();
        } catch (Exception e) {
            throw new EncoderException(e);
        }
    }
}
