package fit.nsu.labs.common.exception;

public class EncoderException extends Exception {
    public EncoderException(Throwable cause) {
        super(cause);
    }

    public EncoderException(String message) {
        super(message);
    }
}
