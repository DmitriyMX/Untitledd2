package fit.nsu.labs.common;


import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public final class User implements Serializable {
        @Serial
        private static final long serialVersionUID = 0L;

        private String name;
        private Integer sessionID;

        public User() {
        }

        public User(String name, Integer sessionID) {
                this.name = name;
                this.sessionID = sessionID;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public Integer getSessionID() {
                return sessionID;
        }

        public void setSessionID(Integer sessionID) {
                this.sessionID = sessionID;
        }

        @Override
        public boolean equals(Object obj) {
                if (obj == this) return true;
                if (obj == null || obj.getClass() != this.getClass()) return false;
                var that = (User) obj;
                return Objects.equals(this.name, that.name) &&
                        Objects.equals(this.sessionID, that.sessionID);
        }

        @Override
        public int hashCode() {
                return Objects.hash(name, sessionID);
        }

        @Override
        public String toString() {
                return "User[" +
                        "name=" + name + ", " +
                        "sessionID=" + sessionID + ']';
        }

}
