package fit.nsu.labs.common.codec;

import fit.nsu.labs.common.dto.Command;
import fit.nsu.labs.common.exception.DecoderException;
import fit.nsu.labs.common.exception.EncoderException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class JaxbClientCodec implements Encoder<Command>, Decoder<Command> {
    private static final Logger log = LoggerFactory.getLogger(JaxbClientCodec.class);

    @Override
    public Command decode(InputStream inputStream) throws DecoderException {
        try {
            var dis = new DataInputStream(inputStream);
            var length = dis.readInt();
            var xmlBytes = dis.readNBytes(length);

            DumpUtil.dumpPacket(xmlBytes, "Dump incoming XML packet without first 4 bytes (length)");

            return (Command) JAXBContext.newInstance(Command.class).createUnmarshaller()
                    .unmarshal(new ByteArrayInputStream(xmlBytes));
        } catch (Exception e) {
            throw new DecoderException(e);
        }
    }

    @Override
    public byte[] encode(Command command) throws EncoderException {
        log.debug("[client.codec] outgoing message {}", command.getClass().getSimpleName());

        try {
            var baos = new ByteArrayOutputStream();

            var marshaller = JAXBContext.newInstance(Command.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(command, baos);

            byte[] bytes = baos.toByteArray();

            baos = new ByteArrayOutputStream();
            var dos = new DataOutputStream(baos);
            dos.writeInt(bytes.length);
            dos.write(bytes);
            dos.flush();

            return baos.toByteArray();
        } catch (Exception e) {
            throw new EncoderException(e);
        }
    }
}
