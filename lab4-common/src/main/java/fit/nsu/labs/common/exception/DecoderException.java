package fit.nsu.labs.common.exception;

public class DecoderException extends Exception {
    public DecoderException(Throwable cause) {
        super(cause);
    }

    public DecoderException(String message) {
        super(message);
    }
}
